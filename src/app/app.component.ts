import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'doughnutproject';
  links = ['Doughnuts', 'Locations', 'Careers', 'Team', 'Order Now']
}
